import { dibujarHorizontal,dibujarVertical } from "./dibujo.js";
const velMov= 10


const eventoTeclado = (e) =>{
    if(e.key==="ArrowLeft"){
        e.preventDefault()
        dibujarHorizontal((velMov)*-1)
    }
    
    if(e.key==="ArrowRight"){
        e.preventDefault()
        dibujarHorizontal(velMov)
    }
    
    if(e.key==="ArrowUp"){
        e.preventDefault()
        dibujarVertical(velMov*-1)
    }
    
    if(e.key==="ArrowDown"){
        e.preventDefault()
        dibujarVertical(velMov)
    }
}




export default eventoTeclado;