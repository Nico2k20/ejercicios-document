let intervalo = null;
let date = new Date()
const segundos = 59
const minutos = 59
const horas = 23
const ano = (date.getFullYear()%4)===0 ? 366 : 365;

const $contador = document.querySelector(".cuenta-regresiva")


const cuentaRegresiva = ()=>{
    date = new Date()
    let segundoActual = (date.getSeconds())
    let minutoActual = (date.getMinutes())
    let horaActual = (date.getHours())
    let diaActual = (date.getDate())

    let restaSegundo = ((segundos - segundoActual)<=9 ? "0"+(segundos - segundoActual) : segundos - segundoActual);
    let restaMinuto = ((minutos - minutoActual)<=9 ? "0"+(minutos - minutoActual) : minutos - minutoActual);
    let restaHora = ((horas - horaActual)<=9 ? "0"+(horas - horaActual) : horas - horaActual);
    let restaAno = ((ano - diaActual)<=9 ? "0"+(ano - diaActual) : ano - diaActual);

    $contador.innerHTML = ("Dia:"+(restaAno) + "  Hora:"+(restaHora)+
    "      Minuto:"+restaMinuto+"      Segundo:"+ restaSegundo)    
    setTimeout(cuentaRegresiva,1000)

}



export const iniciarCuentaRegresiva= ()=>{

    intervalo = setTimeout(cuentaRegresiva,0)

}

