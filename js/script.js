import manejoClick from "./menu.js";
import  alarma  from "./reloj.js"
import {dibujarCuadrado} from "./dibujo.js";
import eventoTeclado from "./evento_teclado.js";
import { iniciarCuentaRegresiva } from "./cuenta_regresiva.js";
import {activarIcono } from "./activarIcono.js";
import { llevarArriba } from "./llevarArriba.js";
import { cambiarEstilo } from "./modoOscuro.js";
const $dom = document;

const $menuNav = document.getElementById("input-nav");
const $nav = document.querySelector(".nav")




$menuNav.addEventListener("click",()=>manejoClick("nav-estilo"))

$dom.addEventListener("click",(e)=> cambiarEstilo(e,".icono1"))

$dom.addEventListener("click",(e)=> llevarArriba(e,".icono"))

$dom.addEventListener("click",(e)=> alarma(e,".btn-reloj-iniciar",".btn-reloj-detener",".btn-alarma-iniciar",".btn-alarma-detener",".reloj"))

$dom.addEventListener("keydown", (e)=> eventoTeclado(e)) 

$dom.addEventListener("scroll",() => activarIcono(".iconoSubir","icono-subir"))

iniciarCuentaRegresiva()

window.onload = dibujarCuadrado