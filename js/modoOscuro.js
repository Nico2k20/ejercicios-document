



const estaEnModoOscuro = ($elem)=>{
    
    return $elem.style.backgroundColor==="rgb(0, 0, 0)";
}





export const cambiarEstilo = (e,clase1)=>{

    if((e.target.matches(clase1))){

        const $dom = document.documentElement;
        const $body = document.body;
        const $header = document.querySelector(".header");


        if(estaEnModoOscuro($body)){
            let variable1 = getComputedStyle($dom).getPropertyValue("--colorLetrasHeader");
            let variable2 = getComputedStyle($dom).getPropertyValue("--colorheader");
            variable1 = "#fff";        
            variable2 = "#000"

            $body.style.backgroundColor = variable1
            $body.style.color = variable2
            $header.style.backgroundColor = variable2;
            $header.style.color = variable1;
        }

        else{

            let variable1 = getComputedStyle($dom).getPropertyValue("--colorLetrasHeader");
            let variable2 = getComputedStyle($dom).getPropertyValue("--colorheader");
            variable1 = "#000";        
            variable2 = "orange"

            $body.style.backgroundColor = variable1
            $body.style.color = variable2
            $header.style.backgroundColor = variable2;
            $header.style.color = variable1;
        
        }
        
}

}


